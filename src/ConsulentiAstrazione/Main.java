package ConsulentiAstrazione;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        InputStreamReader In = new InputStreamReader(System.in);
        BufferedReader Tastiera = new BufferedReader(In);
        float costoTotale = 0;

        Consulente C[];
        C = new Consulente [5];
        C[0] = new Dirigenti("22776", "Ledda", "Giorgia", 2014, 100);
        C[1] = new Dirigenti("22789", "Licheri", "Alessandro", 2010, 100);
        C[2] = new funzionari("556823", "Malica", "Francesco", 2005, 70,80);
        C[3] = new tecnici("516186", "Pintore", "Daniele", 2009, 40, 50);
        C[4] = new tecnici("5gfd56", "Figus", "Giovanni", 2004, 40, 50);

        for (int i = 0; i < C.length; i++) {
            System.out.println("Inserisci il numero delle ore svolte da : " + C[i].getCognome());
            int numero_ore = Integer.parseInt(Tastiera.readLine());
            System.out.println("Il costo di : " + C[i].getCognome() + " e' pari a : " + C[i].calcolaStipendio(numero_ore));
            costoTotale = costoTotale + C[i].calcolaStipendio(numero_ore);
        }

        System.out.println("Il costo totale del progetto e' pari a : "+costoTotale);
    }
}
