package ConsulentiAstrazione;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class tecnici extends Consulente {
    protected float costOrarioInformatici;
    protected float costOrarioElettronici;
    protected String campoCompetenza;
    protected String appartenenza;

    InputStreamReader In = new InputStreamReader(System.in);
    BufferedReader Tastiera = new BufferedReader(In);

    public tecnici(String codice, String cognome, String nome, int annoAssunzione, float costOrarioInformatici, float costOrarioElettronici) throws IOException {
        super(codice, cognome, nome, annoAssunzione);
        this.costOrarioInformatici = costOrarioInformatici;
        this.costOrarioElettronici = costOrarioElettronici;
        System.out.print("Inserisci 'i' se il campo di competenza del tecnico "+getCognome()+" e' informatica e 'e' se il suo campo e' elettronica : ");
        campoCompetenza = Tastiera.readLine();
        System.out.print("Inserisci 'i' se il tecnico "+getCognome()+" e' interno, inserisci 'e' se il tecnico e' esterno : ");
        appartenenza = Tastiera.readLine();
    }

    public float calcolaStipendio(float numerOre) {
        if (campoCompetenza.equals("i") & appartenenza.equals("i")) {
            return (40 * numerOre)+(2019 - annoAssunzione);
        }
        if (campoCompetenza.equals("i") & appartenenza.equals("e")) {
            return (40 * numerOre);
        }
        if (campoCompetenza.equals("e") & appartenenza.equals("i")) {
            return (50 * numerOre)+(2019 - annoAssunzione);
        }
        if (campoCompetenza.equals("e") & appartenenza.equals("e")){
            return (50 * numerOre);
        }
        return -1;
    }

    public float getCostOrarioInformatici() {
        return costOrarioInformatici;
    }

    public void setCostOrarioInformatici(float costOrarioInformatici) {
        this.costOrarioInformatici = costOrarioInformatici;
    }

    public float getCostOrarioElettronici() {
        return costOrarioElettronici;
    }

    public void setCostOrarioElettronici(float costOrarioElettronici) {
        this.costOrarioElettronici = costOrarioElettronici;
    }
}
