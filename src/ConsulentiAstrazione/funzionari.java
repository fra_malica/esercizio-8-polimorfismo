package ConsulentiAstrazione;

public class funzionari extends Consulente{
    protected float costOrarioJunior;
    protected float costOrarioSenior;

    public funzionari(String codice, String cognome, String nome, int annoAssunzione, float costOrarioJunior, float costOrarioSenior) {
        super(codice, cognome, nome, annoAssunzione);
        this.costOrarioJunior = costOrarioJunior;
        this.costOrarioSenior = costOrarioSenior;
    }

    public float calcolaStipendio(float numerOre) {
        if (2019 - annoAssunzione > 10){
            return costOrarioSenior * numerOre;
        } else {
            return costOrarioJunior * numerOre;
        }
    }

    public float getCostOrarioJunior() {
        return costOrarioJunior;
    }

    public void setCostOrarioJunior(float costOrarioJunior) {
        this.costOrarioJunior = costOrarioJunior;
    }

    public float getCostOrarioSenior() {
        return costOrarioSenior;
    }

    public void setCostOrarioSenior(float costOrarioSenior) {
        this.costOrarioSenior = costOrarioSenior;
    }
}
