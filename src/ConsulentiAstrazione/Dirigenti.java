package ConsulentiAstrazione;

public class Dirigenti extends Consulente {
    protected float costOrario;
    public Dirigenti(String codice, String cognome, String nome, int annoAssunzione, float costOrario) {
        super(codice, cognome, nome, annoAssunzione);
        this.costOrario = costOrario;
    }

    public float calcolaStipendio (float numerOre){
        return costOrario * numerOre;
    }

    public float getCostOrario() {
        return costOrario;
    }

    public void setCostOrario(float costOrario) {
        this.costOrario = costOrario;
    }


}
